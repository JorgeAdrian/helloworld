/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework3u4;

/**
 *
 * @author Jorge Adrian Serna Jimenez
 */
public abstract class Tanques {

    private String nombre;

    public Tanques() {

    }
    
    //overwriting
    public void Tipo(){
        System.out.println("Tanque");
    }
    //
    
    
    
    public void disparar() {
        System.out.println("El "+nombre+" esta disparando.");

    }

    public abstract void moverse();

    public void setNombre(String s) {
        nombre = s;

    }

    public String getNombre() {
        return nombre;
    }
}
