/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework3u4;

import java.util.Scanner;

/**
 *
 * @author Jorge Adrian Serna Jimenez 
 */
public class Homework3U4 {

    public static void main(String[] args) { 
        int option;
        Scanner adr = new Scanner(System.in);
        System.out.println("Please choose an option\n 1: Interfaces. \n 2: Abstraction. \n 3: Overwriting. \n 4: Overloading.");
        option = adr.nextInt();
        IS2 i = new IS2();
        Lav25 l = new Lav25();
        switch (option) {
            case 1:
                //Interfaces
                Carro c = new Carro();
                Motocicleta m = new Motocicleta();
                c.Avanzar();
                m.Avanzar();
                //
                break;
            case 2:
                //abstraccion

                i.disparar();
                i.moverse();

                l.disparar();
                l.moverse();
                //
                break;
            case 3:
                //Overwriting

                i.Tipo();
                l.Tipo();
                //
                break;
            case 4:
                //overloading
                Operation o = new Operation();
                System.out.println(o.suma(6, 7));
                System.out.println(o.suma(5.6, 14.2));
                //
                break;
            default: System.out.println("Please choose other option");
                break;
        }
    }

}
